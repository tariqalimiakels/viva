<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->dropColumn('delivery_charge');
        });

        Schema::table('restaurants', function (Blueprint $table) {
            $table->decimal('delivery_charge')->default(0)->after('restaurant_address');
            $table->decimal('min_order')->default(0)->after('delivery_charge');
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->decimal('delivery_charge')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->dropColumn('delivery_charge');
        });

        Schema::table('restaurants', function (Blueprint $table) {
            $table->dropColumn('delivery_charge');
            $table->dropColumn('min_order');
            $table->text('delivery_charge')->after('restaurant_address');;
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn('delivery_charge');
        });
    }
}
