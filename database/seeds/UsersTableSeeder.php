<?php

use App\Events\Inst;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        // Create admin account
        DB::table('users')->insert([
            'usertype' => 'Admin',
            'first_name' => 'Tariq',
            'last_name' => 'Ali',            
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'image_icon' => 'admin-965bf2e0f3108983112bb705d2db4304',
            'remember_token' => str_random(10),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        DB::table('widgets')->insert([
            'footer_widget1_title' => 'About Restaurant',
            'footer_widget1_desc' => 'Are you hungry? and cannot go out due to lockdown? Then foodin is the right place for you! foodin, offers you a long and detailed list of the best restaurants near you. Whether it is a delicious Pizza, Burger, Biryani or any kind of Fast Food you are craving, foodin has an extensive roaster of 50 restaurants. There are plenty of restaurants available for you to order food online with home delivery. foodin is only available in Karachi for now.',
            'footer_widget2_title' => 'List your restaurant on foodin?',
            'footer_widget2_desc' => "Would you like thousands of new customers to taste your amazing food? So would we! It's simple: you can list your menu online, and deliver them to hungry people. Interested? Let's start our partnership today! </br> <a href='/register'>Get Started</a>",
            'footer_widget3_title' => 'Contact Info',
            'footer_widget3_address' => 'Clifton, Karachi - Pakistan',
            'footer_widget3_phone' => '+92 332 2539825',
            'footer_widget3_email' => 'admin@example.com',
            'about_title' => 'About Us',
            'about_desc' => 'Are you hungry? and cannot go out due to lockdown? Then foodin is the right place for you! foodin, offers you a long and detailed list of the best restaurants near you. Whether it is a delicious Pizza, Burger, Biryani or any kind of Fast Food you are craving, foodin has an extensive roaster of 50 restaurants. There are plenty of restaurants available for you to order food online with home delivery. foodin is only available in Karachi for now.',
            'need_help_title' => 'Need Help?',
            'need_help_phone' => '+92 332 2539825',
            'need_help_time' => 'Monday to Sunday 9 AM - 9 PM'
             
        ]);
        
        DB::table('settings')->insert([            
            'site_name' => 'Food Delivery',
            'currency_symbol' => 'Rs.',
            'site_email' => 'admin@admin.com',
            'site_logo' => 'logo.png',
            'site_favicon' => 'favicon.png',
            'site_description' => 'Food Delivery',
            'site_copyright' => '© 2020',
            'home_slide_image1' => 'home_slide_image1.png',
            'home_slide_image2' => 'home_slide_image2.png',
            'home_slide_image3' => 'home_slide_image3.png',
            'page_bg_image' => 'page_bg_image.png',
            'total_restaurant' => '43',
            'total_people_served' => '112',
            'total_registered_users' => '77'
        ]);
         
		DB::table('restaurant_types')->insert([
			'type' => 'Fast Food',
            'type_image' => 'American_1458535213'
		]);
		
		DB::table('restaurant_types')->insert([
            'type' => 'Bar B Q',
            'type_image' => 'Chinese_1458535609'
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Biryani Stuff',
            'type_image' => 'Indian_1458535662'   
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Ghar Ka Khana',
            'type_image' => 'Mexican_1458535796'
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Pizza',
            'type_image' => 'Sushi_1458535621'
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Sweet Cravings',
            'type_image' => 'Thai_1458535292'
        ]); 

        DB::table('restaurant_types')->insert([
            'type' => 'Mix',
            'type_image' => 'Thai_1458535292'
        ]); 
		
       // factory('App\User', 20)->create();
    }
}
