<?php

namespace App\Helpers;

class Helper
{
    public static function getFormattedPrice($price)
    {
        return number_format($price, 2);
    }
}
