<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    protected $table = 'cart';

    protected $fillable = ['item_name', 'item_price','quantity'];


	public $timestamps = false;
 
    public static function getCart($userId){

        $cart = DB::table('cart')
            ->select('cart.*', 'restaurants.restaurant_name')
            ->join('restaurants','restaurants.id','=','cart.restaurant_id')
            ->where('cart.user_id', $userId)->orderBy('cart.id')->get();

        return $cart;
    }

    public static function getDeliveryCharges($userId){

        $deliveryCharge = DB::select("SELECT SUM(delivery_charge) as delivery_charge FROM (SELECT DISTINCT restaurant_id, delivery_charge FROM cart WHERE user_id = {$userId} ) as t");

        if(count($deliveryCharge) > 0){

            return $deliveryCharge[0]->delivery_charge;
        }

        return 0;
    }

    public static function getCartUniqueRestaurants($userId){

        $cart = DB::table('cart')
            ->select('cart.*', 'users.email')
            ->join('restaurants','restaurants.id','=','cart.restaurant_id')
            ->join('users','users.id','=','restaurants.user_id')
            ->where('cart.user_id', $userId)->groupBy('cart.restaurant_id')->get();

        return $cart;
    }
	 
}
