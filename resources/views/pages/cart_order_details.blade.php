@extends("app")

@section('head_title', 'Order Details' .' | '.getcong('site_name') )

@section('head_url', Request::url())

@section("content")

<div class="sub-banner"
  style="background:url({{ URL::asset('upload/'.getcong('page_bg_image')) }}) no-repeat center top;">
  <div class="overlay">
    <div class="container">
      <h1>Your Order Details</h1>
    </div>
  </div>
</div>

<div class="restaurant_list_detail">
  <div class="container">
    <div class="row">

      {!! Form::open(array('url' => 'order_details','class'=>'','id'=>'order_details','role'=>'form')) !!}
      <div class="col-md-8 col-sm-7 col-xs-12">
        <div class="box_style_2" id="order_process">
          <h2 class="inner">Your Order Details</h2>

          <div class="message">
            <!--{!! Html::ul($errors->all(), array('class'=>'alert alert-danger errors')) !!}-->
            @if (count($errors) > 0)
            <div class="alert alert-danger">

              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif

          </div>
          @if(Session::has('flash_message'))
          <div class="alert alert-success">
            {{ Session::get('flash_message') }}
          </div>
          @endif

          <div class="form-group">
            <label>First name</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="{{$user->first_name}}"
              placeholder="First name">
          </div>
          <div class="form-group">
            <label>Last name</label>
            <input type="text" class="form-control" id="last_name" value="{{$user->last_name}}" name="last_name"
              placeholder="Last name">
          </div>
          <div class="form-group">
            <label>Mobile no</label>
            <input type="text" id="mobile" name="mobile" value="{{$user->mobile}}" class="form-control"
              placeholder="mobile">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" id="email" name="email" value="{{$user->email}}" class="form-control"
              placeholder="Your email">
          </div>
          <div class="form-group">
            <label>Your full address</label>
            <input type="text" id="address" name="address" value="{{$user->address}}" class="form-control"
              placeholder="address">
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <label>Comment</label>
              <textarea class="form-control" style="height:150px" placeholder="Comment if any" name="comment"
                id="comment">{{$user->comment}}</textarea>
            </div>
          </div>


        </div>
        <!-- End box_style_1 -->
      </div>

      @include("_particles.sidebar", ["isConfirmOrderPage" => true])
      {{ Form::close() }}
     
    </div>

  </div>
</div>
</div>


@endsection