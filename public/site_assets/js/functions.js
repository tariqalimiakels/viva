function toggleChevron(e) {
    $(e.target).prev(".panel-heading").find("i.indicator").toggleClass("icon_plus_alt2 icon_minus_alt2")
}
$(window).load(function() {
    $("#status").fadeOut(), $("#preloader").delay(250).fadeOut("slow"), $("body").delay(250).css({
        overflow: "visible"
    }), $("#sub_content").addClass("animated zoomIn"), $(window).scroll(), $(".number").each(function() {
        $(this).prop("Counter", 0).animate({
            Counter: $(this).text()
        }, {
            duration: 2e3,
            easing: "swing",
            step: function(e) {
                $(this).text(Math.ceil(e))
            }
        })
    })
}), $(window).scroll(function() {
    "use strict";
    $(this).scrollTop() > 1 ? $("header").addClass("sticky") : $("header").removeClass("sticky")
}), (new WOW).init(), jQuery(function(e) {
    "use strict";

    function i() {
        e(this).css("display", "block");
        var i = e(this).find(".modal-dialog"),
            a = (e(window).height() - i.height()) / 2,
            t = parseInt(i.css("marginBottom"), 10);
        t > a && (a = t), i.css("margin-top", a)
    }
    e(".modal").on("show.bs.modal", i), e(".modal-popup .close-link").click(function(i) {
        i.preventDefault(), e(".modal").modal("hide")
    }), e(window).on("resize", function() {
        e(".modal:visible").each(i)
    })
}), $(window).bind("resize load", function() {
    $(this).width() < 991 ? ($(".collapse#collapseFilters").removeClass("in"), $(".collapse#collapseFilters").addClass("out")) : ($(".collapse#collapseFilters").removeClass("out"), $(".collapse#collapseFilters").addClass("in"))
}), $(".tooltip-1").tooltip({
    html: !0
}), $(".panel-group").on("hidden.bs.collapse shown.bs.collapse", toggleChevron), $("ul#cat_nav li a").on("click", function() {
    $("ul#cat_nav li a.active").removeClass("active"), $(this).addClass("active")
});